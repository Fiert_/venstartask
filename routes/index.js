const express = require('express');
const router = express.Router();

const userRouter = require('./user');
const locationRouter = require('./location');

router.use('/user', userRouter);
router.use('/location', locationRouter);

module.exports = router;
