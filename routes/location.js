const router = require('express').Router();

const controller = require('../controller/locationController');

router.get('/', controller.getLocations);
router.post('/', controller.createLocation);
router.put('/:id', controller.updateLocation);
router.delete('/:id', controller.deleteLocation);

module.exports = router;
