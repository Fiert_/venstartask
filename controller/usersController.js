const {UserModel} = require('../models').models;

module.exports.getUsers = function (req, res, next) {
    return UserModel.findAll({
        order: [
            ['id', 'ASC'],
        ],
    })
        .then((users) => res.json(users))
        .catch((err) => next(err));
};

module.exports.createUser = function (req, res, next) {
    const {first_name, last_name} = req.body;
    return UserModel.create({first_name, last_name})
        .then((user) => res.json(user))
        .catch((err) => next(err));
};

module.exports.updateUser = function (req, res, next) {
    const {id} = req.params;
    const {active} = req.body;
    return UserModel.findById(id)
        .then((user) => user.update({active}))
        .then((updatedUser) => res.json(updatedUser))
        .catch((err) => next(err));
};

module.exports.deleteUser = function (req, res, next) {
    const {id} = req.params;
    return UserModel.findById(id)
        .then((user) => user.destroy())
        .then(() => res.json({message: 'Deleted'}))
        .catch((err) => next(err));
};
