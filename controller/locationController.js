const {LocationModel} = require('../models').models;

module.exports.getLocations = function (req, res, next) {
    return LocationModel.findAll({
        order: [
            ['id', 'ASC'],
        ],
    })
        .then((locations) => res.json(locations))
        .catch((err) => next(err));
};

module.exports.createLocation = function (req, res, next) {
    const {country, address, city} = req.body;
    return LocationModel.create({country, address, city})
        .then((location) => res.json(location))
        .catch((err) => next(err));
};

module.exports.updateLocation = function (req, res, next) {
    const {id} = req.params;
    const {country, address, city, active} = req.body;
    return LocationModel.findById(id)
        .then((location) => location.update({country, address, city, active}))
        .then((updatedLocation) => res.json(updatedLocation))
        .catch((err) => next(err));
};

module.exports.deleteLocation = function (req, res, next) {
    const {id} = req.params;
    return LocationModel.findById(id)
        .then((location) => location.destroy())
        .then(() => res.json({message: 'Deleted'}))
        .catch((err) => next(err));
};
