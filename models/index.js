const Sequelize = require('sequelize');

const {
    database,
    username,
    password,
    options,
} = require('../app/config/parameters').sequelize;

const sequelize = new Sequelize(database, username, password, Object.assign({}, options, {logging: false}));

const userSchema = require('./scehma/user');
const locationSchema = require('./scehma/location');

const models = {
    UserModel: userSchema(sequelize, Sequelize),
    LocationModel: locationSchema(sequelize, Sequelize),
};

module.exports.sequelize = sequelize;
module.exports.Sequelize = Sequelize;
module.exports.models = models;
