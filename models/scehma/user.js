module.exports = (db, DataTypes) => db.define('user', {
    first_name: {
        type: DataTypes.STRING,
        required: true,
    },
    last_name: {
        type: DataTypes.STRING,
        required: true,
    },
    active: {
        type: DataTypes.BOOLEAN,
        defaultValue: false,
    }
});
