module.exports = (db, DataTypes) => db.define('location', {
    country: {
       type: DataTypes.STRING,
        required: true,
    },
    city: {
        type: DataTypes.STRING,
        required: true,
    },
    address: {
        type: DataTypes.STRING,
        required: true,
    },
    active: {
        type: DataTypes.BOOLEAN,
        defaultValue: false,
    }
});
