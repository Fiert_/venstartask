module.exports = [
    {
        model: 'UserModel',
        data: {
            first_name: 'Test',
            last_name: 'User',
            active: true,
        },

    },
    {
        model: 'UserModel',
        data: {
            first_name: 'Another Test',
            last_name: 'Another User',
            active: false,
        },

    },
    {
        model: 'UserModel',
        data: {
            first_name: 'Tester',
            last_name: 'New User',
            active: true,
        },

    },
];
