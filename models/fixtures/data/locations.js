module.exports = [
    {
        model: 'LocationModel',
        data: {
            country: 'Ukraine',
            city: 'Kharkov',
            address: 'Test address',
            active: false,
        },

    },
    {
        model: 'LocationModel',
        data: {
            country: 'Test Country',
            city: 'Test City',
            address: 'New Test address',
            active: true,
        },

    },
    {
        model: 'LocationModel',
        data: {
            country: 'Another Test Country',
            city: 'Another Test City',
            address: 'Another Test address',
            active: true,
        },

    },
];
