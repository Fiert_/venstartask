const sequelizeFixtures = require('sequelize-fixtures');
const {models, sequelize} = require('../../models');
const _ = require('lodash');

const users = require('./data/users');
const locations = require('./data/locations');

const fixtures = [
    ...users,
    ...locations,
];

function run() {
    return sequelize.drop()
        .then(() => sequelize.sync())
        .then(() => sequelizeFixtures.loadFixtures(fixtures, models));
}

const command = _.last(process.argv);
if (command === 'run') {
    run()
        .then(() => {
            console.log('loaded'); // eslint-disable-line no-console
            process.exit(0); // eslint-disable-line no-process-exit
        })
        .catch((err) => {
            console.error(err); // eslint-disable-line no-console
        });
}
module.exports = {
    run,
};
