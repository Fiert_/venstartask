# Venstar CRUD task

> Backend: Express

> Database: PostgreSQL

> Frontend: React

## Build Setup

``` bash
# install dependencies
npm install

# start docker with database
npm run devtest

# start express server
npm run devstart

# run database unpacking
npm run unpacking

# serve with hot reload at localhost:3000
npm run start
```
