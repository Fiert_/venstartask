import React from 'react';
import {
    withStyles,
    Table,
    TableBody,
    TableCell,
    TableHead,
    TableRow,
    Paper,
    Checkbox,
    Button,
    Modal,
    TextField
} from '@material-ui/core';
import {getLocations, updateLocation, deleteLocation, createLocation} from './Locations.action';

const styles = theme => ({
    root: {
        width: '100%',
        marginTop: theme.spacing.unit * 10,
        overflowX: 'auto',
    },
    table: {
        minWidth: 700,
    },
    deleteButton: {
        margin: theme.spacing.unit,
    },
    createButton: {
        margin: theme.spacing.unit,
        backgroundColor: 'lightGreen',
    },
    createLocationButton: {
        margin: theme.spacing.unit,
        backgroundColor: 'lightGreen',
        left: '24%',
    },
    paper: {
        position: 'absolute',
        width: theme.spacing.unit * 50,
        backgroundColor: theme.palette.background.paper,
        boxShadow: theme.shadows[5],
        padding: theme.spacing.unit * 4,
        left: '40%',
        top: '30%',
    },
    textField: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
        width: 200,
    },
    container: {
        display: 'flex',
        flexWrap: 'wrap',
        marginLeft: '60px',
    },
    text: {
        marginLeft: '75px',
    }
});

class Locations extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            locations: [],
            formModal: false,
            country: '',
            city: '',
            address: '',
        }
    }


    componentDidMount() {
        getLocations()
            .then((locations) => this.setState({locations}))
            .catch((err) => err)
    }

    handleCheckboxClick(location) {
        updateLocation(location.id, {active: !location.active})
            .then(() => getLocations())
            .then((locations) => this.setState({locations}))
            .catch((err) => err);
    }

    handleDeleteButton(location) {
        deleteLocation(location.id)
            .then(() => getLocations())
            .then((locations) => this.setState({locations}))
            .catch((err) => err);
    }

    handleCreateButton() {
        const {address, city, country} = this.state;
        createLocation({address, city, country})
            .then(() => this.setState({address: '', city: '', country: '' }))
            .then(() => getLocations())
            .then((locations) => this.setState({locations}))
            .then(() => this.setState({formModal: false}))
            .catch((err) => err);
    }

    handleOpen = () => {
        this.setState({formModal: true});
    };

    handleClose = () => {
        this.setState({formModal: false});
    };

    handleChange = name => event => {
        this.setState({
            [name]: event.target.value,
        });
    };

    render() {
        const {classes} = this.props;
        const {locations, formModal} = this.state;
        return (
            <Paper className={classes.root}>
                <Modal
                    aria-labelledby="simple-modal-title"
                    aria-describedby="simple-modal-description"
                    open={formModal}
                    onClose={this.handleClose}
                >
                    <div className={classes.paper}>
                        <h2 className={classes.text}> Create Location </h2>
                        <form className={classes.container} noValidate autoComplete="off">
                            <TextField
                                id="standard-name"
                                label="Address"
                                className={classes.textField}
                                onChange={this.handleChange('address')}
                                margin="normal"
                                required
                            />
                            <TextField
                                id="standard-name"
                                label="Country"
                                className={classes.textField}
                                onChange={this.handleChange('country')}
                                margin="normal"
                                required
                            />
                            <TextField
                                id="standard-name"
                                label="City"
                                className={classes.textField}
                                onChange={this.handleChange('city')}
                                margin="normal"
                                required
                            />
                        </form>
                        <Button
                            variant="contained"
                            color="primary"
                            className={classes.createLocationButton}
                            onClick={() => this.handleCreateButton()}
                        >
                            Create Location
                        </Button>
                    </div>
                </Modal>
                <Button
                    variant="contained"
                    color="primary"
                    className={classes.createButton}
                    onClick={() => this.handleOpen()}
                >
                    Create
                </Button>
                <Table className={classes.table}>
                    <TableHead>
                        <TableRow>
                            <TableCell>Id</TableCell>
                            <TableCell>Country</TableCell>
                            <TableCell>City</TableCell>
                            <TableCell>Address</TableCell>
                            <TableCell>Active</TableCell>
                            <TableCell/>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {locations.map(row => {
                            return (
                                <TableRow key={row.id}>
                                    <TableCell component="th" scope="row">
                                        {row.id}
                                    </TableCell>
                                    <TableCell>{row.country}</TableCell>
                                    <TableCell>{row.city}</TableCell>
                                    <TableCell>{row.address}</TableCell>
                                    <TableCell><Checkbox checked={row.active}
                                                         onClick={() => this.handleCheckboxClick(row)}/></TableCell>
                                    <TableCell><Button
                                        variant="contained"
                                        color="secondary"
                                        className={classes.deleteButton}
                                        onClick={() => this.handleDeleteButton(row)}>
                                        Delete
                                    </Button></TableCell>
                                </TableRow>
                            );
                        })}
                    </TableBody>
                </Table>
            </Paper>
        );
    }
}

export default withStyles(styles)(Locations);
