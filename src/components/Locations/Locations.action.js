import {get, post, put, httpDelete} from '../../common/httpRequests';

import {LOCATIONS_API} from '../../common/apiRouting';


export const getLocations = () => get(LOCATIONS_API, true)
    .then(res => res.json(res));

export const createLocation = (body) => post(`${LOCATIONS_API}`, body, true)
    .then(res => res.json());

export const updateLocation = (id, active) => put(`${LOCATIONS_API}/${id}`, active, true)
    .then(res => res.json());

export const deleteLocation = (id) => httpDelete(`${LOCATIONS_API}/${id}`, true);
