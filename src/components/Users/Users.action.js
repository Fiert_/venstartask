import {get, post, put, httpDelete} from '../../common/httpRequests';

import {USERS_API} from '../../common/apiRouting';


export const getUsers = () => get(USERS_API, true)
    .then(res => res.json(res));

export const createUser = (body) => post(`${USERS_API}`, body, true)
    .then(res => res.json());

export const updateUser = (id, active) => put(`${USERS_API}/${id}`, active, true)
    .then(res => res.json());

export const deleteUser = (id) => httpDelete(`${USERS_API}/${id}`, true);
