import React from 'react';
import {
    withStyles,
    Card,
    Paper,
    Checkbox,
    Button,
    Modal,
    TextField,
    Divider,
} from '@material-ui/core';
import {getUsers, updateUser, deleteUser, createUser} from './Users.action';

const styles = theme => ({
    root: {
        width: '100%',
        marginTop: theme.spacing.unit * 10,
        overflowX: 'auto',
    },
    deleteButton: {
        margin: theme.spacing.unit,
        marginTop: 20
    },
    createButton: {
        margin: theme.spacing.unit,
        backgroundColor: 'lightGreen',
    },
    createUserButton: {
        margin: theme.spacing.unit,
        backgroundColor: 'lightGreen',
        left: '28%',
    },
    textField: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
        width: 200,
    },
    paper: {
        position: 'absolute',
        width: theme.spacing.unit * 50,
        backgroundColor: theme.palette.background.paper,
        boxShadow: theme.shadows[5],
        padding: theme.spacing.unit * 4,
        left: '40%',
        top: '30%',
    },
    container: {
        display: 'flex',
        flexWrap: 'wrap',
        marginLeft: '60px',
    },
    text: {
        marginLeft: '95px',
    },
    card: {
        minWidth: 275,
    },
    bullet: {
        display: 'inline-block',
        margin: '0 2px',
        transform: 'scale(0.8)',
    },
    title: {
        fontSize: 14,
    },
    pos: {
        marginBottom: 12,
    },
    checkBox: {
        marginTop: 20
    }
});

class Users extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            users: [],
            formModal: false,
            first_name: '',
            last_name: '',
        }
    }


    componentDidMount() {
        getUsers()
            .then((users) => this.setState({users}))
            .catch((err) => err)
    }

    handleCheckboxClick(user) {
        updateUser(user.id, {active: !user.active})
            .then(() => getUsers())
            .then((users) => this.setState({users}))
            .catch((err) => err);
    }

    handleDeleteButton(user) {
        deleteUser(user.id)
            .then(() => getUsers())
            .then((users) => this.setState({users}))
            .catch((err) => err);
    }

    handleCreateButton() {
        const {first_name, last_name} = this.state;
        createUser({first_name, last_name})
            .then(() => this.setState({first_name: '', last_name: ''}))
            .then(() => getUsers())
            .then((users) => this.setState({users}))
            .then(() => this.setState({formModal: false}))
            .catch((err) => err);
    }

    handleOpen = () => {
        this.setState({formModal: true});
    };

    handleClose = () => {
        this.setState({formModal: false});
    };

    handleChange = name => event => {
        this.setState({
            [name]: event.target.value,
        });
    };

    render() {
        const {classes} = this.props;
        const {users, formModal} = this.state;
        return (
            <Paper className={classes.root}>
                <Modal
                    aria-labelledby="simple-modal-title"
                    aria-describedby="simple-modal-description"
                    open={formModal}
                    onClose={this.handleClose}
                >
                    <div className={classes.paper}>
                        <h2 className={classes.text}> Create User </h2>
                        <form className={classes.container} noValidate autoComplete="off">
                            <TextField
                                id="standard-name"
                                label="First Name"
                                className={classes.textField}
                                onChange={this.handleChange('first_name')}
                                margin="normal"
                                required
                            />
                            <TextField
                                id="standard-name"
                                label="Last Name"
                                className={classes.textField}
                                onChange={this.handleChange('last_name')}
                                margin="normal"
                                required
                            />
                        </form>
                        <Button
                            variant="contained"
                            color="primary"
                            className={classes.createUserButton}
                            onClick={() => this.handleCreateButton()}
                        >
                            Create User
                        </Button>
                    </div>
                </Modal>
                <Button
                    variant="contained"
                    color="primary"
                    className={classes.createButton}
                    onClick={() => this.handleOpen()}
                >
                    Create
                </Button>
                {users.map(row => {
                    return (
                        <Card className={classes.card}>
                            <TextField
                                id="standard-read-only-input"
                                label="First Name"
                                value={row.first_name}
                                className={classes.textField}
                                margin="normal"
                                InputProps={{
                                    readOnly: true,
                                }}
                            />
                            <TextField
                                id="standard-read-only-input"
                                label="Last Name"
                                value={row.last_name}
                                className={classes.textField}
                                margin="normal"
                                InputProps={{
                                    readOnly: true,
                                }}
                            />
                            <Checkbox checked={row.active}
                                      onClick={() => this.handleCheckboxClick(row)}
                                      className={classes.checkBox}/>
                            <Button
                                variant="contained"
                                color="secondary"
                                className={classes.deleteButton}
                                onClick={() => this.handleDeleteButton(row)}>
                                Delete
                            </Button>
                            <Divider/>
                        </Card>
                    )
                })}

            </Paper>
        );
    }
}

export default withStyles(styles)(Users);
