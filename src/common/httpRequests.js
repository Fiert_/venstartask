
const completedFetch = (url, options) => fetch(url, options)
    .then(res => {
        if (res.ok) {
            return res;
        }

        return Promise.reject(res);
    })
    .catch(res => {

        return Promise.reject(res.json());
    });

export const get = (url) => {
    const options = {
        method: 'GET',
        headers: {},
    };

    return completedFetch(url, options);
};

export const post = (url, body) => {
    const options = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(body),
    };

    return completedFetch(url, options);
};

export const put = (url, body) => {
    const options = {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(body),
    };
    return completedFetch(url, options);
};

export const httpDelete = (url) => {
    const options = {
        method: 'DELETE',
        headers: {},
    };

    return completedFetch(url, options);
};
