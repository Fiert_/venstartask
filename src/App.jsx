import React from 'react';
import {BrowserRouter, Route} from 'react-router-dom';
import Layout from "./components/Layout/Layout";

class App extends React.Component {
    render() {
        return (
            <BrowserRouter>
                <div>
                    <Route path="/" component={Layout}/>
                </div>
            </BrowserRouter>
        );
    }
}

export default App;
