const {sequelize} = require('../../../models');

let times = 0;
const MAX_WAIT_SECONDS = 30;
function testConnection() {
    if (++times >= MAX_WAIT_SECONDS) {
        return process.exit(1);
    }
    return isConnected()
        .then((status) => {
            if (status === true) {
                process.exit(0);
                return;
            }
            console.log(`${new Date()} - still trying DATABASE`);
            setTimeout(testConnection, 1000);
        });
}

function isConnected() {
    return new Promise((res) => {
        sequelize
            .authenticate()
            .then(() => res(true))
            .catch(() => res(false));
    });
}

testConnection();
